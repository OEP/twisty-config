#!/bin/bash
set -eu

emit() {
    local scatter=$1
    local length=$2

    cat <<EOF
inherits: ['base.yaml']

scatter: ${scatter}

bootstrap:
  dist0: [0, $(bc <<< "scale=2; ${length} / 2")]
  dist1: [0, $(bc <<< "scale=2; ${length} / 2")]
  dist2: [0, $(bc <<< "scale=2; ${length} / 2")]
  sensor:
    radius: ${length}
EOF
}

emit 1 1 > asl011.yaml
emit 1 2 > asl012.yaml
emit 1 4 > asl014.yaml
emit 1 8 > asl018.yaml

emit 1 1 > asl011.yaml
emit 2 1 > asl021.yaml
emit 4 1 > asl041.yaml
emit 8 1 > asl081.yaml
