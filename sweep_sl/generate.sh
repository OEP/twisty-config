#!/bin/bash
set -eu

emit() {
    local scatter=$1
    local length=$2

    cat <<EOF
inherits: ['base.yaml']

scatter: ${scatter}

bootstrap:
  dist0: [0, $(bc <<< "scale=2; ${length} / 2")]
  dist1: [0, $(bc <<< "scale=2; ${length} / 2")]
  dist2: [0, $(bc <<< "scale=2; ${length} / 2")]
  sensor:
    radius: ${length}
EOF
}

emit 1 1 > sl11.yaml
emit 1 2 > sl12.yaml
emit 1 4 > sl14.yaml
emit 1 8 > sl18.yaml

emit 1 1 > sl11.yaml
emit 2 1 > sl21.yaml
emit 4 1 > sl41.yaml
emit 8 1 > sl81.yaml
