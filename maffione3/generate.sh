#!/bin/bash
set -eu

emit() {
    local dist=$1

    cat <<EOF
inherits: ['base.yaml']

bootstrap:
  arclength: [0, $dist]
EOF
}

for d in 20 30 40 50 60 70 80 100; do
    emit $d > maffione3-$d.yaml
done
