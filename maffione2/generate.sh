#!/bin/bash
set -eu

emit() {
    local mu=$1

    cat <<EOF
inherits: ['base.yaml']

weighter:
  cached_integral:
    mu: $mu
EOF
}

emit 0.05 > maffione2-0.05.yaml
emit 0.1 > maffione2-0.1.yaml

emit 0.5 > maffione2-0.5.yaml
emit 1.0 > maffione2-1.0.yaml

emit 5.0 > maffione2-5.0.yaml
emit 10.0 > maffione2-10.0.yaml
