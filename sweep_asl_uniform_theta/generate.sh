#!/bin/bash
set -eu

emit() {
    local scatter=$1
    local length=$2
    local max_arclength=$(bc <<< "scale=2; 10 / ${scatter}")

    cat <<EOF
inherits: ['base.yaml']

scatter: ${scatter}

bootstrap:
  arclength: [0, $max_arclength]
  sensor:
    radius: ${length}
EOF
}

emit 1 2 > asl012.yaml
emit 2 1 > asl021.yaml

#emit 1 4 > asl012.yaml
#emit 4 1 > asl021.yaml

#emit 1 8 > asl012.yaml
#emit 8 1 > asl021.yaml
