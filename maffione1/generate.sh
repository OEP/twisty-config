#!/bin/bash
set -eu

emit() {
    local scatter=$1

    cat <<EOF
inherits: ['base.yaml']

scatter: ${scatter}
EOF
}

emit 0.1 > maffione1-0.1.yaml
emit 0.2 > maffione1-0.2.yaml
emit 0.4 > maffione1-0.4.yaml
emit 0.6 > maffione1-0.6.yaml
emit 0.8 > maffione1-0.8.yaml
emit 1.0 > maffione1-1.0.yaml
